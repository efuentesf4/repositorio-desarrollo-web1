
from django import forms
from .models import ComentarioArticulo, Estudiantes

class ComentarioForm(forms.ModelForm):
    class Meta:
        model = ComentarioArticulo
        fields = '__all__'

class EstudiantesForm(forms.ModelForm):
    class Meta:
        model = Estudiantes
        fields = '__all__'
