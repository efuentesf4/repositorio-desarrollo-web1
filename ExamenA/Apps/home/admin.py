from django.contrib import admin
from .models import Estudiantes, EstudiantesAprobacion, Articulo, EstudiantesComunes, ComentarioArticulo

# Register your models here.

admin.site.register(Estudiantes)
admin.site.register(EstudiantesAprobacion)
admin.site.register(Articulo)
admin.site.register(EstudiantesComunes)
admin.site.register(ComentarioArticulo)