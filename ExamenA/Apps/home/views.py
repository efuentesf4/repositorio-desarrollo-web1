from django.shortcuts import render
from django.views.generic import TemplateView, CreateView
from .forms import ComentarioForm, EstudiantesForm
from django.urls import reverse_lazy

# Create your views here.

class HomeView(TemplateView):
    template_name='index.html'

class InicioView(TemplateView):
    template_name='inicio.html'

class AdministradoresView(TemplateView):
    template_name='Administradores.html'

class EstudiantesView(TemplateView):
    template_name='Estudiantes.html'
    

class CrearComentarioArticuloView(CreateView):
    template_name = 'CrearComentario.html'
    form_class = ComentarioForm
    success_url = reverse_lazy ('home:indexapp')

class AgregarEstudianteView(CreateView):
    template_name = 'AgregarEstudiante.html'
    form_class = EstudiantesForm
    success_url = reverse_lazy ('home:indexapp')