"""EjemploPaginaA URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from .views import HomeView, InicioView, AdministradoresView, EstudiantesView, CrearComentarioArticuloView, AgregarEstudianteView
from Apps.home import views

from Apps.home.views import HomeView

app_name='home'

urlpatterns = [
    path('', HomeView.as_view(), name='indexapp'),
    path('Inicio', InicioView.as_view(), name='inicioapp'),
    path('Administradores', AdministradoresView.as_view(), name='administradoresapp'),
    path('Estudiantes/', EstudiantesView.as_view(), name='estudiantesapp'),
    path('Crear/', CrearComentarioArticuloView.as_view(), name='crearapp'),
    path('Agregar/', AgregarEstudianteView.as_view(), name='agregarapp'),
]