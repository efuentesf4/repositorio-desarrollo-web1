from unittest.util import _MAX_LENGTH
from django.db import models

# Create your models here.

class Estudiantes(models.Model):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=200)
    creacion = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s %s' % (self.nombre,self.apellido)

class EstudiantesAprobacion(models.Model):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=200)
    creacion = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s %s' % (self.nombre,self.apellido)

class Articulo(models.Model):
    nombre = models.CharField(max_length=100)
    creacion = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s' % (self.nombre)

class EstudiantesComunes(models.Model):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=200)
    creacion = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s %s' % (self.nombre,self.apellido)

class ComentarioArticulo(models.Model):
    Comentario = models.CharField(max_length=300)
    EstudiantesComunes = models.ForeignKey(EstudiantesComunes,on_delete=models.CASCADE)
    creacion = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return '%s' % (self.Comentario)